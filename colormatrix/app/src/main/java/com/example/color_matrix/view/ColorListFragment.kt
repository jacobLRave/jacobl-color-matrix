package com.example.color_matrix.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs

import com.example.color_matrix.databinding.FragmentColorListBinding


class ColorListFragment : Fragment() {
    private var _binding: FragmentColorListBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<ColorListFragmentArgs>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentColorListBinding.inflate(inflater, container, false).also {
        _binding = it

    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val numlist = args.color.toString().mapNotNull { it.digitToIntOrNull() }
        binding.rvColors.adapter = ColorDetailAdapter(numlist)

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun navigate(color: Int) {
        findNavController().navigate(
            ColorFragmentDirections.actionColorFragmentToColorListFragment(
                color
            )
        )
    }
}