package com.example.color_matrix.view

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.color_matrix.databinding.ItemColorBinding
import com.google.android.material.card.MaterialCardView

//The adapter manages the list it is the class that is the middle man
//that allows the data being passed to the view to be intractable


class ColorsAdapter(val nav:(Int)->Unit) : RecyclerView.Adapter<ColorsAdapter.ColorsViewHolder>() {

    private var colors = mutableListOf<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorsViewHolder {
        val binding = ItemColorBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return ColorsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ColorsViewHolder, position: Int) {
        val color = colors[position]
        holder.loadColor(color)
        holder.navigateClick().setOnClickListener(){
            nav(color)
        }
    }

    override fun getItemCount(): Int {
        return colors.size
    }

    fun addColors(colors: IntArray) {
        this.colors = colors.toMutableList()
        notifyDataSetChanged()
    }


    class ColorsViewHolder(
        private val binding: ItemColorBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadColor(color: Int) {
            Log.i("color","$color")
            binding.mvcContainer.setCardBackgroundColor(color)
        }
        fun navigateClick():MaterialCardView{
          return binding.mvcContainer
        }
    }

}