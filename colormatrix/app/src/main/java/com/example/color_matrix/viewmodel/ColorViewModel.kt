package com.example.color_matrix.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.color_matrix.model.ColorRepo

class ColorViewModel : ViewModel() {

    private val repo = ColorRepo

    private val _randomColor = MutableLiveData<IntArray>()
    val randomColor: LiveData<IntArray> get() = _randomColor

    fun getRandomColor(size:Int) {
        val randomColor = repo.getRandomColor(size)
        _randomColor.value = randomColor
    }

}