package com.example.color_matrix.view

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.color_matrix.databinding.FragmentColorBinding
import com.example.color_matrix.viewmodel.ColorViewModel


class ColorFragment : Fragment() {
    private var _binding: FragmentColorBinding? = null
    private val binding get() = _binding!!
    private val colorViewModel by viewModels<ColorViewModel>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentColorBinding.inflate(inflater, container, false).also {
        _binding = it

    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnSubmit.setOnClickListener {
            with(binding.etSize) {
                colorViewModel.getRandomColor(text.toString().toInt())
            }
        }
        colorViewModel.randomColor.observe(viewLifecycleOwner) { colors ->
            binding.rvList.apply {
                layoutManager = GridLayoutManager(context, 3)
                adapter = ColorsAdapter(::navigate).apply {
                    addColors(colors)

                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun navigate(color: Int) {
        findNavController().navigate(
            ColorFragmentDirections.actionColorFragmentToColorListFragment(
                color
            )
        )
    }
}