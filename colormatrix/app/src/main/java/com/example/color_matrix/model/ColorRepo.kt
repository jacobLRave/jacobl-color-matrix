package com.example.color_matrix.model

import com.example.color_matrix.model.remote.ColorApi
import  com.example.color_matrix.model.remote.randomColor

object ColorRepo {

    private val colorApi = object : ColorApi {
        override fun color(): Int {
            return randomColor
        }
    }

    fun getRandomColor(size:Int): IntArray {

        return IntArray(size) {
            colorApi.color()
        }
    }

}