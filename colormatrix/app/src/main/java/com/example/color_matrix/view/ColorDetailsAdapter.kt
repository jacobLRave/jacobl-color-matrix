package com.example.color_matrix.view

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import com.example.color_matrix.databinding.ItemColorDetailBinding


class ColorDetailAdapter(
    private val numberList: List<Int>
) : RecyclerView.Adapter<ColorDetailAdapter.ColorDetailViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ) = ColorDetailViewHolder.getInstance(parent)

    override fun onBindViewHolder(holder: ColorDetailViewHolder, position: Int) {
        holder.bindColor(numberList[position])
    }

    override fun getItemCount() = numberList.size

    class ColorDetailViewHolder(
        private val binding: ItemColorDetailBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindColor(num: Int) {
            binding.root.text = num.toString()
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemColorDetailBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { ColorDetailViewHolder(it) }
        }
    }
}
