package com.example.color_matrix.model.remote

interface ColorApi {

    fun color(): Int

}